import 'package:flutter/material.dart';
import 'package:ar_store/screens/splash/components/body.dart';
import 'package:ar_store/size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ar_store/screens/home/home_screen.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';

class SplashScreen extends StatelessWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  static String routeName = "/splash";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          _auth.authStateChanges().listen((user) {
            if (FirebaseAuth.instance.currentUser != null) {
              Navigator.pushReplacementNamed(context, HomeScreen.routeName);
            } else {
              Navigator.pushReplacementNamed(context, SignInScreen.routeName);
            }
          });
        },
        label: const Text('          Next          '),
        icon: const Icon(Icons.forward),
        backgroundColor: new Color(0xFFFF7643),
      ),
    );
  }
}

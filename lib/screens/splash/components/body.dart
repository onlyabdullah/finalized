
import 'package:flutter/material.dart';
import 'package:ar_store/constants.dart';
import 'package:ar_store/size_config.dart';

// This is the best practice
import '../components/splash_content.dart';
import 'package:ar_store/enums.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> splashData = [
    {
      "text": "Welcome to AR Store, Let’s shop!",
      "image": "assets/images/splash_1.png"
    },
    {
      "text": "We help people to shop \nby Experiecing Products",
      "image": "assets/images/splash_2.png"
    },
    {
      "text": "We show the easy way to shop. \nJust stay at home with us",
      "image": "assets/images/splash_3.png"
    },
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            /*Padding(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      onPressed: () async {
                        bool visitingFlag = await getvisitingFlag();
                        setvisitingFlag();
                        if (visitingFlag == true){
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => checkAuthentication()));
                        }else {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SplashScreen()));
                        }
                      },
                      child: Text(
                        'Skip',
                        style: TextStyle(
                          color: kPrimaryColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),*/
            Expanded(
              flex: 4,
              child: PageView.builder(
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: splashData.length,
                itemBuilder: (context, index) => SplashContent(
                  image: splashData[index]["image"],
                  text: splashData[index]['text'],
                ),
              ),
            ),
            Expanded(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(20)),
                  child: Column(children: <Widget>[
                    Spacer(flex: 1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    Spacer(flex: 3),
                    /*FloatingActionButton(
                      onPressed: () => this.checkAuthentication(),                       
                      child: const Icon(Icons.forward),
                      backgroundColor: new Color(0xFFFF7643),
                    ),*/
                    Spacer(),
                  ]),
                ))
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}


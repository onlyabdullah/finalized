import 'package:ar_store/screens/Support/callCenter.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/screens/chat/chat.dart';
import 'package:ar_store/screens/notifications_page.dart';
import 'package:ar_store/screens/page_track.dart';
import 'package:ar_store/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'profile_menu.dart';
import 'package:ar_store/screens/payment/payment_page.dart';
import 'package:ar_store/screens/profile/profilescreens.dart';
import 'package:ar_store/screens/settings/settings_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';
import 'package:ar_store/admin/screens/admin.dart';
import 'package:ar_store/models/userModel.dart';
import 'profile_pic.dart';

class Body extends StatelessWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  
  final googles = GoogleSignIn();

  @override
  Widget build(BuildContext context) {    

    return SingleChildScrollView(
        child: Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: kToolbarHeight),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: RichText(
              text: TextSpan(
                style: Theme.of(context)
                    .textTheme
                    // ignore: deprecated_member_use
                    .title
                    .copyWith(fontWeight: FontWeight.w900),
                children: [
                  TextSpan(
                    text: "AR",
                    style: TextStyle(color: kSecondaryColor),
                  ),
                  TextSpan(
                    text: " STORE",
                    style: TextStyle(color: kPrimaryColor),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            margin: EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: kPrimaryColor,
                      blurRadius: 4,
                      spreadRadius: 1,
                      offset: Offset(0, 1))
                ]),
            height: 150,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: SvgPicture.asset(
                          "assets/icons/truck.svg",
                          color: kPrimaryColor,
                          width: 30,
                        ),
                        onPressed: () {
                          Navigator.popAndPushNamed(
                              context, TrackingPage.routeName);
                        },
                      ),
                      Text(
                        'Delivery',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: SvgPicture.asset(
                          "assets/icons/card.svg",
                          color: kPrimaryColor,
                          width: 30,
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PaymentPage()));
                        },
                      ),
                      Text(
                        'Payment',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: SvgPicture.asset(
                          "assets/icons/support.svg",
                          color: kPrimaryColor,
                          width: 30,
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => CallCenter()));
                        },
                      ),
                      Text(
                        'Support',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          ProfileMenu(
            text: "My Account",
            icon: "assets/icons/User Icon.svg",
            press: () => {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ProfileScreens())),
            },
          ),
          ProfileMenu(
            text: "Admin Panel",
            icon: "assets/icons/User Icon.svg",
            press: () => {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Admin())),
            },
          ),
          ProfileMenu(
            text: "Messages",
            icon: "assets/icons/Chat bubble Icon.svg",
            press: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Chat()));
            },
          ),
          ProfileMenu(
            text: "Settings",
            icon: "assets/icons/Settings.svg",
            press: () {
              Navigator.popAndPushNamed(context, SettingsPage.routeName);
            },
          ),
          ProfileMenu(
            text: "Log Out",
            icon: "assets/icons/Log out.svg",
            press: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.pushReplacementNamed(context, SignInScreen.routeName);
            },
          ),
        ],
      ),
    ));
  }
}

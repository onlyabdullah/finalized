import 'package:ar_store/models/category.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/constants.dart';
import 'components/staggered_category_card.dart';

import '../../constants.dart';
import '../../size_config.dart';

class CategoryListPage extends StatefulWidget {
  static String routeName = '/CategoryListPage';
  @override
  _CategoryListPageState createState() => _CategoryListPageState();
}

class _CategoryListPageState extends State<CategoryListPage> {
  List<Category> categories = [
    Category(
      Color(0xffFCE183),
      Color(0xffF68D7F),
      'Pants',
      'assets/images/jeans_4.png',
    ),
    Category(
      Color(0xffF749A2),
      Color(0xffFF7375),
      'Clothes',
      'assets/images/tshirt.png',
    ),
    Category(
      Color(0xff00E9DA),
      Color(0xff5189EA),
      'Fashion',
      'assets/images/shorts.png',
    ),
    Category(
      Color(0xffAF2D68),
      Color(0xff632376),
      'Headphones',
      'assets/images/headphones.png',
    ),
    Category(
      Color(0xff36E892),
      Color(0xff33B2B9),
      'Watch',
      'assets/images/w1.png',
    ),
    Category(
      Color(0xffF123C4),
      Color(0xff668CEA),
      'Shoes',
      'assets/images/shoes2.png',
    ),
  ];

  List<Category> searchResults;
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchResults = categories;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Category List"),
        ),
        body: Material(
          child: Container(
            margin: const EdgeInsets.only(top: 0.0),
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment(-1, 0),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                  ),
                ),
                Container(
                  width: SizeConfig.screenWidth * 0.9,
                  decoration: BoxDecoration(
                    color: kSecondaryColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: TextField(
                    controller: searchController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(20),
                            vertical: getProportionateScreenWidth(9)),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        hintText: "Search",
                        prefixIcon: Icon(Icons.search)),
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        // ignore: deprecated_member_use
                        List<Category> tempList = List<Category>();
                        categories.forEach((category) {
                          if (category.category.toLowerCase().contains(value)) {
                            tempList.add(category);
                          }
                        });
                        setState(() {
                          searchResults.clear();
                          searchResults.addAll(tempList);
                        });
                        return;
                      } else {
                        setState(() {
                          searchResults.clear();
                          searchResults.addAll(categories);
                        });
                      }
                    },
                  ),
                ),
                Flexible(
                  child: ListView.builder(
                    itemCount: searchResults.length,
                    itemBuilder: (_, index) => Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 16.0,
                      ),
                      child: StaggeredCardCard(
                        begin: searchResults[index].begin,
                        end: searchResults[index].end,
                        categoryName: searchResults[index].category,
                        assetPath: searchResults[index].image,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

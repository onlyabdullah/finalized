import 'package:flutter/material.dart';
import 'package:ar_store/screens/home/home_screen.dart';
import 'package:ar_store/size_config.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ar_store/constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.screenHeight * 0.04),
        Image.asset(
          "assets/images/success.png",
          height: SizeConfig.screenHeight * 0.4, //40%
        ),
        SizedBox(height: SizeConfig.screenHeight * 0.08),
        Text(
          "Login Success",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(30),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Spacer(),
        /* FloatingActionButton(
                      onPressed: () async {
                        Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HomeScreen()));
                      },
                      child: const Icon(Icons.forward),
                      backgroundColor: new Color(0xFFFF7643),
                    ),*/
        Spacer(),
      ],
    );
  }
}

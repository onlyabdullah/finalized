import 'package:flutter/material.dart';
import 'package:ar_store/screens/home/home_screen.dart';
import 'components/body.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';

class SignupSuccessScreen extends StatelessWidget {
  static String routeName = "/signup_success";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        title: Text("Signup Success"),
      ),
      body: Body(),
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
         Navigator.pushReplacementNamed(context, SignInScreen.routeName);
        },
        label: const Text('          Next          '),
        backgroundColor: new Color(0xFFFF7643),
      ),
    );
  }
}

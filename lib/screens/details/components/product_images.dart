import 'package:flutter/material.dart';
import 'package:ar_store/models/Product.dart';

import '../../../constants.dart';
import '../../../models/3dmodel.dart';
import '../../../size_config.dart';

class ProductImages extends StatefulWidget {
  const ProductImages({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _ProductImagesState createState() => _ProductImagesState();
}

class _ProductImagesState extends State<ProductImages> {
  int selectedImage = 0;
  int index = 1;
  final List<String> model = [
    "chair",
    "sofaset",
    "sofa"
    
    
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: widget.product.id.toString(),
              child: Image.asset(widget.product.picture[selectedImage]),
            ),
          ),
        ),
        // SizedBox(height: getProportionateScreenWidth(20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...List.generate(widget.product.picture.length,
                (index) => buildSmallProductPreview(index)),
          AnimatedContainer(
        duration: defaultDuration,
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          boxShadow: [
      BoxShadow(
        color: Colors.grey[400],
        blurRadius: 5.0,
      ),
    ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: Colors.green[200]),
        ),
        child: IconButton(
            icon: const Icon(Icons.view_in_ar),
            tooltip: 'View in Your Space',
            onPressed: () {
              Navigator.of(context).pushReplacement(
                 MaterialPageRoute(builder: (ctx) => Model(model[widget.product.id.bitLength])));
            }),
      ),
          ],
        )
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(8),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: Image.asset(widget.product.picture[index]),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../models/Product.dart';
import 'components/body.dart';
import 'components/custom_app_bar.dart';
import 'package:ar_store/constants.dart';
import 'package:ar_store/models/3dmodel.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = "/details";

  @override
  Widget build(BuildContext context) {
    final ProductDetailsArguments agrs =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: CustomAppBar(rating: agrs.product.rating),      
     /* floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 445.0),
        child: FloatingActionButton(
          backgroundColor: kWhiteColor,
          child: Icon(
            Icons.view_in_ar_sharp,
            color: Colors.black,
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
          onPressed:(){
            Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (ctx) => DetailsScreen()));
          },
        ),
      ),*/
      body: Body(product: agrs.product),
    );
  }
}

class ProductDetailsArguments {
  final Product product;

  ProductDetailsArguments({@required this.product});
}

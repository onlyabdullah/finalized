import 'package:flutter/material.dart';
import 'components/body.dart';


class PasswordChanged extends StatelessWidget {
  static String routeName = "/PasswordChanged";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        title: Text("Password Changed"),
      ),
      body: Body(),
    );
  }
}

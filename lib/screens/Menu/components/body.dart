import 'package:ar_store/admin/screens/admin.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';
import 'package:ar_store/screens/settings/settings_page.dart';
import 'package:ar_store/screens/notifications_page.dart';
import 'package:ar_store/screens/faq_page.dart';
import 'package:ar_store/screens/about/about.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ar_store/components/notificationList.dart';
import 'package:ar_store/admin/screens/admin.dart';

import 'menu_tiles.dart';
import 'package:ar_store/constants.dart';

class Body extends StatelessWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final googles = GoogleSignIn();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF9F9F9),
      body: SafeArea(
        top: true,
        child: SingleChildScrollView(
        child: Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: kToolbarHeight),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                style: Theme.of(context)
                    .textTheme
                    // ignore: deprecated_member_use
                    .title
                    .copyWith(fontWeight: FontWeight.w900),
                children: [
                  TextSpan(
                    text: "AR",
                    style: TextStyle(color: kSecondaryColor),
                  ),
                  TextSpan(
                    text: " STORE",
                    style: TextStyle(color: kPrimaryColor),
                  ),
                ],
              ),
            ),
          ),Padding(
            padding: const EdgeInsets.all(15.0),
          ),
          
          MenuTiles(
            text: "Admin Panel",
            icon: "assets/icons/Bell.svg",
            press: () {
              Navigator.popAndPushNamed(context, Admin.routeName);
            },
          ),
          MenuTiles(
            text: "Settings",
            icon: "assets/icons/Settings.svg",
            press: () {
              Navigator.popAndPushNamed(context, SettingsPage.routeName);
            },
          ),
          MenuTiles(
            text: "Help Center",
            icon: "assets/icons/Question mark.svg",
            press: () {
              Navigator.popAndPushNamed(context, FaqPage.routeName);
            },
          ),
          MenuTiles(
            text: "Customer Support",
            icon: "assets/icons/support.svg",
            press: () {
              Navigator.popAndPushNamed(context, FaqPage.routeName);
            },
          ),
          MenuTiles(
            text: "FAQ's",
            icon: "assets/icons/faqs.svg",
            press: () {
              Navigator.popAndPushNamed(context, FaqPage.routeName);
            },
          ),
          MenuTiles(
            text: "App Info",
            icon: "assets/icons/info.svg",
            press: () => {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => About())),
            },
          ),
          MenuTiles(
            text: "Log Out",
            icon: "assets/icons/Log out.svg",
            press: () async {
              await FirebaseAuth.instance.signOut();
              await googles.disconnect();
            Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (ctx) => SignInScreen()));
            },
          ),
        ],
      ),
    ))));
  }
}

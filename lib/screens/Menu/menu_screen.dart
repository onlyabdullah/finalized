import 'package:flutter/material.dart';

import 'components/body.dart';

class MenuScreen extends StatelessWidget {
  static String routeName = "/MenuScreen";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
      ),
      body: Body(),
    );
  }
}

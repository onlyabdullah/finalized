/*import 'package:ar_store/models/products_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/models/search.dart';
import 'package:ar_store/models/single_product.dart';
import 'package:ar_store/models/Product.dart';
import 'package:provider/provider.dart';
import 'package:ar_store/screens/details/details_screen.dart';
class SearchProduct extends SearchDelegate<void> {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }
  @override
  Widget buildSuggestions(BuildContext context) {
    ProductProvider providerProvider = Provider.of<ProductProvider>(context);
    List<Product> searchCategory = providerProvider.searchProductList(query);
    return GridView.count(
        childAspectRatio: 0.76,
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: searchCategory
            .map((e) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (ctx) => DetailsScreen(
                  
                        ),
                      ),
                    );
                  },
                  child: SingleProduct(
                    title: e.title,
                    price: e.price,
                  ),
                ))
            .toList());
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }
}
 /* @override
  Widget buildResults(BuildContext context) {
    ProductProviders providerProvider = Provider.of<ProductProviders>(context);
   List<Product> searchCategory = providerProvider.searchProductList(query);

    return GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: searchCategory
            .map((e) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (ctx) => DetailsScreen(
                        ),
                      ),
                    );
                  },
                  child: SingleProduct(
                    title: e.title,
                    price: e.price,
                  ),
                ))
            .toList());
  }
*/*/
import 'package:ar_store/screens/Menu/menu_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ar_store/screens/notifications_page.dart';
import '../../../size_config.dart';
import 'package:ar_store/constants.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          /*IconButton(
            icon: SvgPicture.asset("assets/icons/menu.svg"),
            onPressed: () {
              Navigator.pushNamed(context, MenuScreen.routeName);
            },
          ),*/
          RichText(
            text: TextSpan(
              style: Theme.of(context)
                  .textTheme
                  .title
                  .copyWith(fontWeight: FontWeight.w900),
              children: [
                TextSpan(
                  text: "AR",
                  style: TextStyle(color: kSecondaryColor),
                ),
                TextSpan(
                  text: " STORE",
                  style: TextStyle(color: kPrimaryColor),
                ),
              ],
            ),
          ),
          IconButton(
            icon: SvgPicture.asset("assets/icons/Bell.svg"),
            onPressed: () {
              Navigator.pushNamed(context, NotificationsPage.routeName);
            },
          ),
        ],
      ),
    );
  }
}

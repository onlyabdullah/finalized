import 'package:flutter/material.dart';
import 'package:ar_store/components/coustom_bottom_nav_bar.dart';
import 'package:ar_store/enums.dart';
import 'package:ar_store/size_config.dart';

import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}

import 'package:ar_store/screens/chat/chat.dart';
import '../../../components/default_button.dart';
import 'package:ar_store/constants.dart';
import 'package:ar_store/screens/chat/emptySection.dart';
import 'package:ar_store/subTitle.dart';
import 'package:flutter/material.dart';

class CallCenter extends StatefulWidget {
  static String routeName = '/callCenter';
  CallCenter({Key key}) : super(key: key);

  @override
  _CallCenterState createState() => _CallCenterState();
}

class _CallCenterState extends State<CallCenter> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: Text("Chat"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          EmptySection(
            emptyImg: callCenter,
            emptyMsg: "We're happy to help you!",
          ),
          SubTitle(
            subTitleText: "If you have complain about the product chat us",
          ),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: DefaultButton(
              text: "Chat Me",
              press: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Chat(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

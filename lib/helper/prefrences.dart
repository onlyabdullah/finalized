import 'package:ar_store/models/Product.dart';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SqliteHelper {
  static Database _database;
  static SqliteHelper _databaseHelper;

  //Projection
  static final String AppDatabase = "sameedchutia.db";

  // projection for cart & wishlist table
  static final String _itemID = 'id';
  static final String name = 'name';
  static final String desc = 'desc';
  static final String quantity = 'quantity';
  static final String rating = 'rating';
  static final String price = 'price';
  static final String fav = 'fav';
  static final String pop = 'pop';
  static final String imgs = 'imgs';
  static final String colors = 'colors';

  // table
  static final String _TableCart = 'cart';

  SqliteHelper._createInstance();

  factory SqliteHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = SqliteHelper._createInstance();
    }
    return _databaseHelper;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + AppDatabase;
    var database = openDatabase(path,
        version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return database;
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion != newVersion) {
      await db.execute("DROP TABLE IF EXISTS $_TableCart");
      await db.execute(
          "CREATE TABLE $_TableCart ($_itemID INTEGER ,$name TEXT,$desc TEXT,$quantity INTEGER,$rating TEXT,$price TEXT,$fav INTEGER,$pop INTEGER,$imgs TEXT,$colors TEXT )");
    }
  }

  void _onCreate(Database db, int currentVersion) async {
    await db.execute(
        "CREATE TABLE $_TableCart ($_itemID INTEGER ,$name TEXT,$desc TEXT,$quantity INTEGER,$rating TEXT,$price TEXT,$fav INTEGER,$pop INTEGER,$imgs TEXT,$colors TEXT )");
  }

  Future<bool> addProductToCart(Product product) async {
    var db = await database;
    var mappedProduct = product.toMap();
  
    return await db.insert(_TableCart, mappedProduct) > 0;
  }

  void updateProductFromCart(Product product) async {
    var db = await database;
    var mappedProduct = product.toMap();
    if (product.quantity > 0) {
      await db.update(_TableCart, mappedProduct,
          where: '$_itemID = ?', whereArgs: [product.id]);
    } else {
      await db
          .delete(_TableCart, where: '$_itemID = ?', whereArgs: [product.id]);
    }
  }

  Future<List<Product>> getAllCartProducts() async {
    var db = await database;
    // List<Map<String, dynamic>> queryResult = await db.rawQuery('SELECT * FROM $_TableCart');
    List<Map<String, dynamic>> queryResult = await db.query(_TableCart);
    List<Product> products = List();
    queryResult.forEach((map) {
      Product product = Product.fromCartMap(map);
      print(map.toString());
      products.add(product);
    });
    return products;
  }

  void clearCart() async {
    var db = await database;
    await db.rawQuery('DELETE FROM $_TableCart');
  }
}

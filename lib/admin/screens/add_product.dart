import 'dart:io';

import 'package:ar_store/models/Product.dart';
import 'package:ar_store/models/products_provider.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/models/Product.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '/../models/add_product.dart';
import 'package:ar_store/models/app_states.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ar_store/constants.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  ProductService productService = ProductService();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController titleController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  final ratingController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController idController = TextEditingController();
  bool isPopular = false;
  bool isFavourite = false;
  Color white = Colors.white;
  Color black = Colors.black;
  Color grey = Colors.grey;
  Color red = Colors.red;
  List<String> colors = <String>[];

  File _image1;
  File _image2;
  File _image3;
  bool isLoading = false;

  @override
  void initState() {
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final productProvider = Provider.of<ProductProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Product"),
        backgroundColor: kTransparent,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: isLoading
              ? CircularProgressIndicator()
              : Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: OutlineButton(
                                borderSide: BorderSide(
                                    color: grey.withOpacity(0.5), width: 2.5),
                                onPressed: () {
                                  getImage(
                                      source: ImageSource.gallery,
                                      imageNumber: 1);
                                },
                                child: _displayChild1()),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: OutlineButton(
                                borderSide: BorderSide(
                                    color: grey.withOpacity(0.5), width: 2.5),
                                onPressed: () {
                                  getImage(
                                      source: ImageSource.gallery,
                                      imageNumber: 2);
                                },
                                child: _displayChild2()),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: OutlineButton(
                              borderSide: BorderSide(
                                  color: grey.withOpacity(0.5), width: 2.5),
                              highlightElevation: 10,
                              onPressed: () {
                                getImage(
                                    source: ImageSource.gallery,
                                    imageNumber: 3);
                              },
                              child: _displayChild3(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12)                           
                          ),
                    Text('Available Colors'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('red')) {
                                productProvider.removeColor('red');
                              } else {
                                productProvider.addColors('red');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('red')
                                      ? Colors.blue
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: Colors.red,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('yellow')) {
                                productProvider.removeColor('yellow');
                              } else {
                                productProvider.addColors('yellow');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('yellow')
                                      ? red
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: Colors.yellow,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('blue')) {
                                productProvider.removeColor('blue');
                              } else {
                                productProvider.addColors('blue');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('blue')
                                      ? red
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: Colors.blue,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('green')) {
                                productProvider.removeColor('green');
                              } else {
                                productProvider.addColors('green');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('green')
                                      ? red
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: Colors.green,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('white')) {
                                productProvider.removeColor('white');
                              } else {
                                productProvider.addColors('white');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('white')
                                      ? red
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: white,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              if (productProvider.selectedColors
                                  .contains('black')) {
                                productProvider.removeColor('black');
                              } else {
                                productProvider.addColors('black');
                              }
                              setState(() {
                                colors = productProvider.selectedColors;
                              });
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: productProvider.selectedColors
                                          .contains('black')
                                      ? red
                                      : grey,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: CircleAvatar(
                                  backgroundColor: black,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text('Popular'),
                            SizedBox(
                              width: 10,
                            ),
                            Switch(
                                value: isPopular,
                                onChanged: (value) {
                                  setState(() {
                                    isPopular = value;
                                  });
                                }),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Favourite'),
                            SizedBox(
                              width: 10,
                            ),
                            Switch(
                                value: isFavourite,
                                onChanged: (value) {
                                  setState(() {
                                    isFavourite = value;
                                  });
                                }),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'enter a product name with 10 characters at maximum',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: red, fontSize: 12),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: idController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Product ID",
                          hintText: 'Product ID',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'You must enter the product name';
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                            labelText: "Product Name",
                            hintText: 'Product name'),
                        // ignore: missing_return
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'You must enter the product name';
                          } else if (value.length < 10) {
                            return 'Product name cant have less than 10 letters';
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: priceController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Product Price",
                          hintText: 'Price',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'You must enter the product name';
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: ratingController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Product Rating",
                          hintText: 'Rating',
                        ),
                        // ignore: missing_return
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Rating of Product';
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: descriptionController,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          labelText: "Product Name",
                          hintText: 'Product Description',
                        ),
                        // ignore: missing_return
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Description of Product';
                          }
                        },
                      ),
                    ),
                    FlatButton(
                      color: red,
                      textColor: white,
                      child: Text('add product'),
                      onPressed: () {
                        validateAndUpload();
                      },
                    )
                  ],
                ),
        ),
      ),
    );
  }

  File _pickedImage;
  PickedFile _image;
  Future<void> getImage({ImageSource source, int imageNumber}) async {
    _image = await ImagePicker().getImage(source: source);
    File tempImg = _pickedImage;
    switch (imageNumber) {
      case 1:
      if (_image != null) {
      setState(() {
        _image1 = File(_image.path);
      });
    }        
        break;
      case 2:
      if (_image != null) {
      setState(() {
        _image2 = File(_image.path);
      });
    }  
        break;
      case 3:
       if (_image != null) {
      setState(() {
        _image3 = File(_image.path);
      });
    }  
        break;
    }
  }

  Widget _displayChild1() {
    if (_image1 == null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 55, 14, 55),
        child: new Icon(
          Icons.add,
          color: grey,
        ),
      );
    } else {
      return Image.file(
        _image1,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  Widget _displayChild2() {
    if (_image2 == null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 55, 14, 55),
        child: new Icon(
          Icons.add,
          color: grey,
        ),
      );
    } else {
      return Image.file(
        _image2,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  Widget _displayChild3() {
    if (_image3 == null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 55, 14, 55),
        child: new Icon(
          Icons.add,
          color: grey,
        ),
      );
    } else {
      return Image.file(
        _image3,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  void validateAndUpload() async {
    if (_formKey.currentState.validate()) {
      setState(() => isLoading = true);
      if (_image1 != null) {
        String imageUrl1;
        String imageUrl2;
        String imageUrl3;

        final FirebaseStorage storage = FirebaseStorage.instance;
        final String picture1 =
            "1${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
        UploadTask task1 = storage.ref().child(picture1).putFile(_image1);
        final String picture2 =
            "2${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
        UploadTask task2 = storage.ref().child(picture2).putFile(_image2);
        final String picture3 =
            "3${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
        UploadTask task3 = storage.ref().child(picture3).putFile(_image3);

        TaskSnapshot snapshot1 = await task1.then((snapshot) => snapshot);
        TaskSnapshot snapshot2 = await task2.then((snapshot) => snapshot);

        task3.then((snapshot3) async {
          imageUrl1 = await snapshot1.ref.getDownloadURL();
          imageUrl2 = await snapshot2.ref.getDownloadURL();
          imageUrl3 = await snapshot3.ref.getDownloadURL();
          List<String> imageList = [imageUrl1, imageUrl2, imageUrl3];
          productService.uploadProduct({
            "name": titleController.text,
            "price": double.parse(priceController.text),
            "rating": double.parse(ratingController.text),
            "colors": colors,
            "picture": imageList,
            "description": descriptionController.text,
            'isPopuplar': isPopular,
            'isFavourite': isFavourite,
          });
          _formKey.currentState.reset();
          setState(() => isLoading = false);
          Fluttertoast.showToast(msg: 'Product added');
          Navigator.pop(context);
        });
      } else {
        setState(() => isLoading = false);
        Fluttertoast.showToast(msg: 'select atleast one size');
      }
    } else {
      setState(() => isLoading = false);

      Fluttertoast.showToast(msg: 'all the images must be provided');
    }
  }
}

import 'package:flutter/material.dart';
import 'add_product.dart';
import 'package:ar_store/models/add_categroy.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'showProducts.dart';
import 'users.dart';
import 'small_card.dart';
import 'package:ar_store/constants.dart';

enum Page { dashboard, manage }

class Admin extends StatefulWidget {
  static String routeName = "/Admin";
  @override
  _AdminState createState() => _AdminState();
}

class _AdminState extends State<Admin> {
  Page _selectedPage = Page.dashboard;
  MaterialColor active = Colors.red;
  MaterialColor notActive = Colors.grey;
  TextEditingController categoryController = TextEditingController();
  TextEditingController brandController = TextEditingController();
  GlobalKey<FormState> _categoryFormKey = GlobalKey();
  GlobalKey<FormState> _brandFormKey = GlobalKey();
  CategoryService _categoryService = CategoryService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Expanded(
                  child: FlatButton.icon(
                      onPressed: () {
                        setState(() => _selectedPage = Page.dashboard);
                      },
                      icon: Icon(
                        Icons.dashboard,
                        color: _selectedPage == Page.dashboard
                            ? active
                            : notActive,
                      ),
                      label: Text('Dashboard'))),
              Expanded(
                  child: FlatButton.icon(
                      onPressed: () {
                        setState(() => _selectedPage = Page.manage);
                      },
                      icon: Icon(
                        Icons.sort,
                        color:
                            _selectedPage == Page.manage ? active : notActive,
                      ),
                      label: Text('Manage'))),
            ],
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: _loadScreen());
  }

  Widget _loadScreen() {
    switch (_selectedPage) {
      case Page.dashboard:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 40),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: 'Revenue\n',
                      style: TextStyle(fontSize: 35, color: Colors.grey)),
                  TextSpan(
                      text: '\$1287.99',
                      style: TextStyle(
                          fontSize: 55,
                          color: Colors.black,
                          fontWeight: FontWeight.w300)),
                ]),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SmallCard(
                  color2: Colors.indigo,
                  color1: Colors.blue,
                  icon: Icons.person_outline,
                  value: 1265,
                  title: 'Users',
                ),
                SmallCard(
                  color2: Colors.indigo,
                  color1: Colors.blue,
                  icon: Icons.shopping_cart,
                  value: 30,
                  title: 'Orders',
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SmallCard(
                  color2: Colors.black87,
                  color1: Colors.black87,
                  icon: Icons.attach_money,
                  value: 65,
                  title: 'Sales',
                ),
                SmallCard(
                  color2: Colors.black,
                  color1: Colors.black87,
                  icon: Icons.shopping_basket,
                  value: 230,
                  title: 'Stock',
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SmallCard(
                  color2: Colors.orange,
                  color1: Colors.red,
                  icon: Icons.cancel,
                  value: 6,
                  title: 'Returns',
                ),
                SmallCard(
                  color2: Colors.green,
                  color1: Colors.greenAccent,
                  icon: Icons.branding_watermark,
                  value: 230,
                  title: 'Brands',
                ),
              ],
            ),
          ],
        );
        break;
      case Page.manage:
        return ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.add),
              title: Text("Add product"),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => AddProduct()));
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.production_quantity_limits_outlined),
              title: Text("Products list"),
              onTap: () {
                Navigator.push(
                context, MaterialPageRoute(builder: (_) => ShowProduct()));
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.person_outlined),
              title: Text("Users"),
              onTap: () {
                Navigator.push(
                context, MaterialPageRoute(builder: (_) => ShowUsers()));
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () async {
                await FirebaseAuth.instance.signOut();
                Navigator.pushReplacementNamed(context, SignInScreen.routeName);
              },
            ),
            Divider(),
          ],
        );
        break;
      default:
        return Container();
    }
  }

  void _categoryAlert() {
    var alert = new AlertDialog(
      content: Form(
        key: _categoryFormKey,
        child: TextFormField(
          controller: categoryController,
          validator: (value) {
            if (value.isEmpty) {
              return 'category cannot be empty';
            }
          },
          decoration: InputDecoration(hintText: "add category"),
        ),
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              if (categoryController.text != null) {
                _categoryService.createCategory(categoryController.text);
              }
//          Fluttertoast.showToast(msg: 'category created');
              Navigator.pop(context);
            },
            child: Text('ADD')),
        FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('CANCEL')),
      ],
    );

    showDialog(context: context, builder: (_) => alert);
  }

  void _brandAlert() {
    var alert = new AlertDialog(
      content: Form(
        key: _brandFormKey,
        child: TextFormField(
          controller: brandController,
          validator: (value) {
            if (value.isEmpty) {
              return 'category cannot be empty';
            }
          },
          decoration: InputDecoration(hintText: "add brand"),
        ),
      ),
    );

    showDialog(context: context, builder: (_) => alert);
  }
}


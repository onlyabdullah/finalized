import 'package:flutter/material.dart';

class UserModel {
  String firstName, lastName, phoneNumber, address, email, image;
  UserModel(
      {@required this.email,
      @required this.image,
      @required this.firstName,
      @required this.lastName,
      @required this.phoneNumber,
      @required this.address});
}

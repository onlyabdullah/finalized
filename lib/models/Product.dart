import 'dart:convert';

import 'package:ar_store/screens/home/components/popular_product.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  int id;
  String name, description;
  List<String> picture;
  List<Color> colors;
  double rating, price;
  bool isFavourite, isPopular;
  int quantity;

  Product({
    @required this.id,
    @required this.picture,
    @required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    @required this.name,
    @required this.price,
    @required this.description,
  });

  Product.fromCartMap(var map) {
    this.id = map['id'];
    this.name = map['name'];
    this.description = map['desc'];
    this.rating = double.parse(map['rating']);
    this.price = double.parse(map['price']);
    this.isFavourite = map['fav'] == 1;
    this.isPopular = map['pop'] == 1;
    this.picture = List.from(jsonDecode((map['imgs'])).toList());
    this.quantity = map['quantity'];
    this.colors = List.from(jsonDecode(map['colors']))
        .map((e) => Color(int.parse(e)))
        .toList();
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map();
    map['id'] = this.id;
    map['name'] = this.name;
    map['desc'] = this.description;
    map['rating'] = this.rating.toString();
    map['price'] = this.price.toString();
    map['fav'] = this.isFavourite ? 1 : 0;
    map['pop'] = this.isPopular ? 1 : 0;
    map['quantity'] = this.quantity;
    map['ímgs'] = jsonEncode(picture).toString();
    map['colors'] =
        jsonEncode(colors.map((e) => e.value.toString()).toList()).toString();
    return map;
  }
}

// Our demo Products
//DEMo

List<Product> demoProducts = [
  Product(
    id: 0,
    picture: [
      "assets/images/w1.png",
      "assets/images/w2.png",
      "assets/images/w3.png",
      "assets/images/w4.png"
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    name: "Office Chair",
    price: 84.99,
    description: "Office Chair with beautiful color.",
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 1,
    picture: [
      "assets/images/t1.png",
      "assets/images/t2.png",
      "assets/images/t3.png",
      "assets/images/t4.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    name: "Moden Full Sofa",
    price: 30.5,
    description: "Full Seat Sofa 4 seat capacity.",
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 2,
    picture: [
      "assets/images/s1.png",
      "assets/images/s2.png",
      "assets/images/s3.png",
      "assets/images/s4.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    name: "Modern Single Sofa",
    price: 50.5,
    description: "New Design Single Sofa Designed with Leather.",
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 4,
    picture: [
      "assets/images/glove.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    name: "Gloves XC Omega - Polygon",
    price: 36.55,
    description: "Best Gym Gloves for workout made with Leather",
    rating: 4.1,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 3,
    picture: [
      "assets/images/wireless headset.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    name: "Logitech Head",
    price: 20.20,
    description: "Awesome Headphones",
    rating: 4.1,
    isFavourite: true,
  ),
];

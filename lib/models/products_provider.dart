import 'package:flutter/material.dart';
import 'productModel.dart';
import 'product_services.dart';

class ProductProvider with ChangeNotifier {
  List<String> selectedColors = [];
  ProductServices _productServices = ProductServices();
  List<ProductModel> products = [];
  List<ProductModel> productsSearched = [];


  loadProducts()async{
    products = await _productServices.getProducts();
    notifyListeners();
  }

  Future search({String productName})async{
    productsSearched = await _productServices.searchProducts(productName: productName);
    notifyListeners();
  }
 
  addColors(String color) {
    selectedColors.add(color);
    print(selectedColors.length.toString());
    notifyListeners();
  }

  removeColor(String color) {
    selectedColors.remove(color);
    print(selectedColors.length.toString());
    notifyListeners();
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class ProductModel {
  static const ID = "id";
  static const NAME = "name";
  static const PICTURE = "picture";
  static const PRICE = "price";
  static const DESCRIPTION = "description";
  static const ISFAVOURITE = "isFavourite";
  static const ISPOPULAR = "isPopular";
  static const RATING = "rating";
  static const COLORS = "colors";

  int _id;
  String _name;
  List<String> _picture;
  String _description;
  double _price;
  bool _isFavourite;
  bool _isPopular;
  List<String> _colors;
  double _rating;

  int get id => _id;

  String get name => _name;

  List get picture => _picture;

  String get description => _description;

  double get quantity => _rating;

  double get price => _price;

  bool get isFavourite => _isFavourite;

  bool get isPopular => _isPopular;

  List get colors => _colors;

  ProductModel.fromSnapshot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    _id = data[ID];
    _name = data[NAME];
    _rating = data[RATING];
    _description = data[DESCRIPTION] ?? " ";
    _price = data[PRICE];
    _isFavourite = data[ISFAVOURITE];
    _colors = data[COLORS];
    _isPopular = data[ISPOPULAR];
    _picture = data[PICTURE];
  }
}

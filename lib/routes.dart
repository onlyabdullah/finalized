import 'package:flutter/widgets.dart';
import 'package:ar_store/screens/cart/cart_screen.dart';
import 'package:ar_store/screens/complete_profile/complete_profile_screen.dart';
import 'package:ar_store/screens/details/details_screen.dart';
import 'package:ar_store/screens/forgot_password/forgot_password_screen.dart';
import 'package:ar_store/screens/home/home_screen.dart';
import 'package:ar_store/screens/login_success/login_success_screen.dart';
import 'package:ar_store/screens/signup_success/signup_success_screen.dart';
import 'package:ar_store/screens/otp/otp_screen.dart';
import 'package:ar_store/screens/profile/profile_screen.dart';
import 'package:ar_store/screens/sign_in/sign_in_screen.dart';
import 'package:ar_store/screens/splash/splash_screen.dart';
import 'package:ar_store/screens/category/category_list_page.dart';
import 'package:ar_store/screens/settings/settings_page.dart';
import 'package:ar_store/screens/notifications_page.dart';
import 'package:ar_store/screens/passwordchanged/passwordchanged.dart';
import 'package:ar_store/screens/page_track.dart';
import 'screens/sign_up/sign_up_screen.dart';
import 'package:ar_store/screens/faq_page.dart';
import 'package:ar_store/screens/Menu/menu_screen.dart';
import 'screens/rating/rating_page.dart';
import 'package:ar_store/screens/Support/callCenter.dart';
import 'package:ar_store/screens/chat/chat.dart';
import 'package:ar_store/screens/about/about.dart';
import 'screens/chat/emptySection.dart';
import 'package:ar_store/components/notificationList.dart';
import 'screens/payment/payment_page.dart';
import 'screens/profile/profilescreens.dart';
import 'package:ar_store/admin/screens/admin.dart';
import 'package:ar_store/models/3dmodel.dart';


// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  SignupSuccessScreen.routeName: (context) => SignupSuccessScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  CategoryListPage.routeName: (context) => CategoryListPage(),
  SettingsPage.routeName: (context) => SettingsPage(),
  NotificationsPage.routeName: (context) => NotificationsPage(),
  PasswordChanged.routeName: (context) => PasswordChanged(),
  TrackingPage.routeName: (context) => TrackingPage(),
  FaqPage.routeName: (context) => FaqPage(),
 // ProfilePage.routeName: (context) => ProfilePage(),
  MenuScreen.routeName: (context) => MenuScreen(),
  RatingPage.routeName: (context) => RatingPage(),
  PasswordChanged.routeName: (context) => PasswordChanged(),
  Chat.routeName: (context) => Chat(),
  CallCenter.routeName: (context) => CallCenter(),
  EmptySection.routeName: (context) => EmptySection(),
  About.routeName: (context) => About(),
  NotificationList.routeName: (context) => NotificationList(),
  PaymentPage.routeName: (context) => PaymentPage(),
  ProfileScreens.routeName : (context) => ProfileScreens(),
  Admin.routeName : (context) => Admin(),
};

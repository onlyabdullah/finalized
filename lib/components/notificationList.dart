import 'package:ar_store/constants.dart';
import 'package:flutter/material.dart';
import '/components/notificationTiles.dart';
import 'package:ar_store/screens/notifications_page.dart';

class NotificationList extends StatefulWidget {   
  static String routeName = '/notificationList';
  NotificationList({Key key}) : super(key: key);

  @override
  _NotificationListState createState() => _NotificationListState();
}

class _NotificationListState extends State<NotificationList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: Text('Notifications'),
      ),
      body: ListView.separated(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.zero,
          itemCount: 3,
          itemBuilder: (context, index) {
            return NotificationTiles(
              title: 'AR Store',
              subtitle: 'Thanks for download AR Store app.',
              enable: true,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => NotificationsPage())),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          }),
    );
  }
}

import 'package:ar_store/models/Product.dart';
import 'package:flutter/material.dart';
import 'package:ar_store/routes.dart';
import 'package:ar_store/screens/splash/splash_screen.dart';
import 'package:ar_store/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:ar_store/screens/home/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:ar_store/models/products_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

int initScreen = 0;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  initScreen = await prefs.getInt("initScreen");
  await prefs.setInt("initScreen", 1);
 
   await Firebase.initializeApp();

  runApp(MyApp());
  
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
    providers: [
      ChangeNotifierProvider.value(value: ProductProvider()),
      ChangeNotifierProvider<ProductProvider>(
          create: (context) => ProductProvider(),
        ),
    ],    
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themes(),
      home: HomeScreen(),
       initialRoute: SplashScreen.routeName,
      routes: routes,

      // We use routeName so that we dont need to remember the name

      
    ),
    );
  }
}

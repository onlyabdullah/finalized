# Summary

Date : 2021-06-23 00:09:36

Directory d:\BSSE 2017\FYP\ar_store\test

Total : 1 files,  14 codes, 10 comments, 7 blanks, all 31 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 1 | 14 | 10 | 7 | 31 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 1 | 14 | 10 | 7 | 31 |

[details](details.md)
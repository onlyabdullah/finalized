# Summary

Date : 2021-06-23 00:09:20

Directory d:\BSSE 2017\FYP\ar_store\ios

Total : 7 files,  222 codes, 2 comments, 9 blanks, all 233 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 2 | 145 | 0 | 2 | 147 |
| XML | 2 | 61 | 2 | 2 | 65 |
| Swift | 1 | 12 | 0 | 2 | 14 |
| Markdown | 1 | 3 | 0 | 2 | 5 |
| C++ | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 7 | 222 | 2 | 9 | 233 |
| Runner | 7 | 222 | 2 | 9 | 233 |
| Runner\Assets.xcassets | 3 | 148 | 0 | 4 | 152 |
| Runner\Assets.xcassets\AppIcon.appiconset | 1 | 122 | 0 | 1 | 123 |
| Runner\Assets.xcassets\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| Runner\Base.lproj | 2 | 61 | 2 | 2 | 65 |

[details](details.md)
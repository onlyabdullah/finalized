# Summary

Date : 2021-06-23 00:12:46

Directory d:\BSSE 2017\FYP\ar_store\android

Total : 13 files,  214 codes, 42 comments, 30 blanks, all 286 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Groovy | 4 | 87 | 3 | 22 | 112 |
| XML | 6 | 79 | 38 | 6 | 123 |
| JSON | 1 | 39 | 0 | 0 | 39 |
| Properties | 2 | 9 | 1 | 2 | 12 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 13 | 214 | 42 | 30 | 286 |
| app | 7 | 141 | 41 | 18 | 200 |
| app\src | 5 | 52 | 38 | 6 | 96 |
| app\src\debug | 1 | 4 | 3 | 1 | 8 |
| app\src\main | 3 | 44 | 32 | 4 | 80 |
| app\src\main\res | 2 | 13 | 16 | 3 | 32 |
| app\src\main\res\drawable | 1 | 4 | 7 | 2 | 13 |
| app\src\main\res\values | 1 | 9 | 9 | 1 | 19 |
| app\src\profile | 1 | 4 | 3 | 1 | 8 |
| gradle | 1 | 5 | 1 | 1 | 7 |
| gradle\wrapper | 1 | 5 | 1 | 1 | 7 |

[details](details.md)
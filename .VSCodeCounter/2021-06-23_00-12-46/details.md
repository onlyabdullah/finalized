# Details

Date : 2021-06-23 00:12:46

Directory d:\BSSE 2017\FYP\ar_store\android

Total : 13 files,  214 codes, 42 comments, 30 blanks, all 286 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 50 | 3 | 12 | 65 |
| [android/app/google-services.json](/android/app/google-services.json) | JSON | 39 | 0 | 0 | 39 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | 31 | 16 | 1 | 48 |
| [android/app/src/main/res/drawable/launch_background.xml](/android/app/src/main/res/drawable/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [android/app/src/profile/AndroidManifest.xml](/android/app/src/profile/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android/ar_store_android.iml](/android/ar_store_android.iml) | XML | 27 | 0 | 0 | 27 |
| [android/build.gradle](/android/build.gradle) | Groovy | 28 | 0 | 5 | 33 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 4 | 0 | 1 | 5 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 5 | 1 | 1 | 7 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 8 | 0 | 4 | 12 |
| [android/settings_aar.gradle](/android/settings_aar.gradle) | Groovy | 1 | 0 | 1 | 2 |

[summary](results.md)